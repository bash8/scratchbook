backup_files="var/log"
dest="/BackupFiles"
timestamp=$(date +%Y-%m-%d-%H%M%S)
hostname=$(hostname -s)
filescount=$(ls $dest | wc -l)
archive_file="$hostname-$timestamp.tar.bz2"
finalcount=7

while [ $filescount -ge $finalcount ] ; do 
 oldestfile=$(find $dest -type f -printf '%T+ %p\n' | sort | head -n 1)
 rm -f $oldestfile
 let filescount=filescount-1
done

echo "Start Backup $backup_files"
  date
echo

  tar -cvpzf $dest/$archive_file $backup_files

echo
echo "THE END"
  date

ls -lh $des